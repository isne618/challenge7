#ifndef STRVAR_H
#define STRVAR_H

#include <iostream>

using namespace std;

namespace strvarken
{
	class StringVar
	{
	public:
		StringVar(int size);
		StringVar();
		StringVar(const char a[]);

		StringVar(const StringVar& string_object);
		~StringVar();
		char one_char(int location);//select location of character that user want to print
		void set_char(int location, char w);//select location of character that user want to change
		int length() const;
		void input_line(istream& ins);
		friend ostream& operator<<(ostream& outs, const StringVar& the_string);//overloaded operator <<
		friend istream& operator>>(istream& ins, const StringVar& the_string); //overloaded operator >>
		friend StringVar operator + (StringVar word, StringVar word2); //make 2 words connect to each other
		friend bool operator == (StringVar word, StringVar word2); //check 2 words are the same or not
		StringVar copypiece(int start, int end); //select location of charater that use want to start copy

	private:
		char *value;
		int max_length;
	};
}

#endif //STRVAR_H