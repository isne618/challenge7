#include <iostream>
#include "strvar.h"

void conversation(int max_name_size);

int main()
{
	using namespace std;
	conversation(30);
	cout << "End of Demo.\n";

	system("cmd /c pause");
	return 0;
}


//Demo Func
void conversation(int max_name_size)
{
	using namespace std;
	using namespace strvarken;

	StringVar your_name(max_name_size), our_name("Borg");
	cout << "What is your name?\n";
	//cin >> your_name;
	your_name.input_line(cin);
	
	cout << "We are " << our_name << endl;
	cout << "We will meet again " << your_name << endl;

	StringVar word(max_name_size);
	StringVar new_word(max_name_size);
	StringVar word2(max_name_size);
	int location, start = 0, end = 0;
	char w;
	cout << "Input something: ";
	word.input_line(cin);
	cout << "Which location do you want to print: ";
	cin >> location;
	cout << "---> " << word.one_char(location) << endl;
	cout << "Which location do you want to change character: ";
	cin >> location; 
	cout << "Input character: ";
	cin >> w;
	word.set_char(location, w);
	cout << "---> " << word << endl;
	cout << "Input word that you want to add: ";
	word2.input_line(cin);//the cin from above land in this input
	word2.input_line(cin);//ugly fix
	cout << "---> " << word + word2 << endl; //connect 2 words together
	if (word == word2)
	{
		cout << word << " = " << word2 << endl; //if 2 words are the same
	}
	else
	{
		cout << word << " != " << word2 << endl; //if 2 words are not the same
	}

	cout << "string to use: " << word+word2 << endl; //word that use for copy
	cout << "What is your first location that you want to copy: ";
	cin >> start;
	cout << "What is your last location that you want to copy: ";
	cin >> end;
	cout << "copy ---> " << (word+word2).copypiece(start, end) << endl;
	//system("pause");
}