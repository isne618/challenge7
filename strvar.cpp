#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <cstring>
#include "strvar.h"

using namespace std;

namespace strvarken
{
	//Uses cstddef and cstdlib
	StringVar::StringVar(int size) : max_length(size)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstddef and cstdlib
	StringVar::StringVar() : max_length(100)
	{
		value = new char[max_length + 1];
		value[0] = '\0';
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const char a[]) : max_length(strlen(a))
	{
		value = new char[max_length + 1];

		for(int i = 0;i < strlen(a);i++)
		{
			value[i] = a[i];
		}
		value[strlen(a)] = '\0';
	}

	//Uses cstring, cstddef and cstdlib
	StringVar::StringVar(const StringVar& string_object) : max_length(string_object.length())
	{
		value = new char[max_length + 1];
		for(int i = 0;i < strlen(string_object.value);i++)
		{
			value[i] = string_object.value[i];
		}
		value[strlen(string_object.value)] = '\0';
	}

	StringVar::~StringVar()
	{
		delete [] value;
	}

	//Uses cstring
	int StringVar::length() const
	{
		return strlen(value);
	}


	//Uses iostream
	void StringVar::input_line(istream& ins)
	{
		ins.getline(value, max_length + 1);
	}

	StringVar StringVar::copypiece(int start, int end)
	{
		start--;
		end--;

		StringVar cpy(max_length);
		int i;
		for (i = 0; i < end - start; i++)
		{
			cpy.value[i] = value[start + i];
		}
		cpy.value[strlen(cpy.value)] = '\0';
		return cpy;
	}

	bool operator == (StringVar word, StringVar word2)
	{
		int i;
		if (word.length() == word2.length())
		{
			for (i = 0; i < word.length(); i++)
			{
				if (word.value[i] == word2.value[i])
				{
					return false;
				}
			}
			return true;
		}
		else
		{
			return false;
		}
	}
	StringVar operator + (StringVar word, StringVar word2)
	{
		StringVar new_word;
		int i, j;
		for (i = 0; i < word.length(); i++)
		{
			new_word.value[i] = word.value[i];
		}
		for (j = word.length(); j <= (word2.length() + word.length()); j++)
		{
			if (j == word2.length() + word.length())
			{
				new_word.value[j] = '\0';
				break;
			}
			new_word.value[j] = word2.value[j - word.length()];
		}
		return new_word;
	}

	//Uses iostream
	ostream& operator << (ostream& outs, const StringVar& the_string)
	{
		outs << the_string.value;
		return outs;
	}

	istream& operator >> (istream& ins, const StringVar& the_string)
	{
		ins >> the_string.value;
		return ins;
	}

	char StringVar::one_char(int location)
	{
		return value[location - 1];
	}

	void StringVar::set_char(int location, char w)
	{
		value[location - 1] = w;
	}

}

	

//Retyped By CharlZKP - #Tired